mod lib;
use std::io::prelude::*;
use std::str::FromStr;

struct PasswordRule {
    min_count: u32,
    max_count: u32,
    letter: char,
}

impl PasswordRule {
    fn validate(&self, password: &str) -> bool {
        let mut count = 0;
        for c in password.chars() {
            if c == self.letter {
                count += 1;
            }
        }
        return self.min_count <= count && count <= self.max_count;
    }

    fn validate_weird(&self, password: &str) -> bool {
        let chars: Vec<char> = password.chars().collect();
        return (chars[self.min_count as usize - 1] == self.letter)
            ^ (chars[self.max_count as usize - 1] == self.letter);
    }
}

impl FromStr for PasswordRule {
    type Err = std::num::ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = input.split(' ').collect();
        let range: Vec<&str> = parts[0].split("-").collect();
        let min_count = range[0].parse::<u32>()?;
        let max_count = range[1].parse::<u32>()?;
        let letter = parts[1].chars().next().unwrap();

        Ok(PasswordRule {
            min_count,
            max_count,
            letter,
        })
    }
}

fn parse_input() -> Vec<(PasswordRule, String)> {
    let mut vec = Vec::new();

    for mline in lib::load("02").unwrap().lines() {
        let line = mline.unwrap();
        let parts: Vec<&str> = line.split(':').collect();
        let rule = PasswordRule::from_str(parts[0]).unwrap();
        let password = parts[1].trim();
        vec.push((rule, password.to_string()))
    }

    vec
}

fn main() {
    let input = parse_input();
    println!(
        "Valid passwords: {}",
        input
            .iter()
            .filter(|(rule, password)| rule.validate(password))
            .count()
    );
    println!(
        "Valid weird passwords: {}",
        input
            .iter()
            .filter(|(rule, password)| rule.validate_weird(password))
            .count()
    );
}
