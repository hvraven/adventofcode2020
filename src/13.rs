use std::io::BufRead;

mod lib;

fn main() {
    let mut lines = lib::load("13").unwrap().lines();
    let time = lines.next().unwrap().unwrap().parse::<u64>().unwrap();
    let busses = lines
        .next()
        .unwrap()
        .unwrap()
        .split(",")
        .map(|bus| bus.parse::<u64>().ok())
        .collect::<Vec<_>>();

    let (next_bus, wait_time) = busses
        .iter()
        .filter_map(|obus| obus.map(|bus| (bus, bus - (time % bus))))
        .min_by_key(|(_, time)| *time)
        .unwrap();
    println!(
        "Next bus with ID {} in {} minutes: {}",
        next_bus,
        wait_time,
        next_bus * wait_time
    );

    let rules = busses
        .iter()
        .filter_map(|x| *x)
        .zip(0..)
        .collect::<Vec<_>>();

    let (first, _) = rules.iter().fold((0, 1), |(mut t, incr), (bus, offset)| {
        while (t + offset) % bus != 0 {
            t += incr
        }
        (t, (incr * bus))
    });
    println!("{}", first)
}
