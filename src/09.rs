mod lib;
use itertools::Itertools;
use std::io::BufRead;

fn load_xmas() -> Vec<u64> {
    lib::load("09")
        .unwrap()
        .lines()
        .map(|l| l.unwrap().parse::<u64>().unwrap())
        .collect()
}

fn check_xmas(code: &Vec<u64>) -> &u64 {
    code.as_slice()
        .windows(25)
        .zip(code.iter().skip(25))
        .find(|(window, current)| {
            !window
                .iter()
                .combinations(2)
                .any(|v| v.iter().fold(0, |a, b| a + **b) == **current)
        })
        .unwrap()
        .1
}

fn exploit_xmas(code: &Vec<u64>, error: &u64) -> u64 {
    for window_size in 2..code.len() {
        if let Some(found) = code
            .windows(window_size)
            .find(|window| window.iter().sum::<u64>() == *error)
        {
            return found.iter().min().unwrap() + found.iter().max().unwrap();
        }
    }
    0
}

fn main() {
    let numbers = load_xmas();
    println!("Loaded {} lines of code", numbers.len());
    let error = check_xmas(&numbers);
    println!("First invalid number is {}", error);
    let weakness = exploit_xmas(&numbers, error);
    println!("The encryption weakness is {}", weakness)
}
