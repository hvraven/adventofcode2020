mod lib;
use std::io::BufRead;

fn import() -> Vec<u32> {
    lib::load("10")
        .unwrap()
        .lines()
        .map(|s| s.unwrap().parse::<u32>().unwrap())
        .collect()
}

fn full_adapter_chain(mut converters: Vec<u32>) -> Vec<u32> {
    converters.push(0);
    converters.push(*converters.iter().max().unwrap() + 3);
    converters.sort();
    converters
        .iter()
        .zip(converters.iter().skip(1))
        .map(|(a, b)| b - a)
        .collect()
}

fn use_all(distance: &Vec<u32>) -> usize {
    [1, 3]
        .iter()
        .map(|jolts| distance.iter().filter(|a| *a == jolts).count())
        .product::<usize>()
}

fn all_combinations(distance: &Vec<u32>) -> usize {
    distance
        .split(|e| *e == 3)
        .map(|block| match block.len() {
            0 => 1,
            1 => 1,
            2 => 1 + 1,
            3 => 1 + 2 + 1,
            4 => 1 + 4 + 2,
            _ => panic!("unknown block length"),
        })
        .product()
}

fn main() {
    let converters = import();
    let full_chain = full_adapter_chain(converters);
    println!("Jolt 1 3 differences: {}", use_all(&full_chain));
    println!(
        "All possible combinations: {}",
        all_combinations(&full_chain)
    );
}
