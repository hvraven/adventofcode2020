mod lib;
use std::io::prelude::*;

type Trees = Vec<Vec<bool>>;

fn load_trees() -> Trees {
    let mut rows = Vec::new();
    for line in lib::load("03").unwrap().lines() {
        rows.push(line.unwrap().chars().map(|c| c == '#').collect());
    }
    rows
}

fn hit_trees(trees: &Trees, right: usize, down: usize) -> u64 {
    let mut pos = 0;
    let mut hits = 0;
    for col in trees.iter().step_by(down) {
        if col[pos % col.len()] {
            hits += 1;
        }
        pos += right;
    }
    hits
}

fn optimize_hitting_trees(trees: &Trees) -> u64 {
    [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|(right, down)| hit_trees(&trees, *right, *down))
        .fold(1, |a, b| a * b)
}

fn main() {
    let trees = load_trees();
    println!("Hit {} trees on first run", hit_trees(&trees, 3, 1));
    println!(
        "Hit {} trees on optimised runs",
        optimize_hitting_trees(&trees)
    );
}
