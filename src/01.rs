mod lib;
use std::io::prelude::*;

fn main() {
    let vec = get_numbers();
    println!("{}", part1(&vec));
    println!("{}", part2(&vec));
}

fn get_numbers() -> Vec<i32> {
    let mut vec = Vec::new();

    for line in lib::load("01").unwrap().lines() {
        vec.push(line.unwrap().parse().unwrap())
    }

    vec
}

fn part1(input: &Vec<i32>) -> i32 {
    let mut vec = Vec::new();
    for num in input {
        for v in &vec {
            if num + v == 2020 {
                return v * num;
            }
        }
        vec.push(*num)
    }

    0
}

fn part2(input: &Vec<i32>) -> i32 {
    let mut vec = Vec::new();
    for num in input {
        for v in &vec {
            for u in &vec {
                if num + v + u == 2020 {
                    return u * v * num;
                }
            }
        }
        vec.push(*num)
    }

    0
}
