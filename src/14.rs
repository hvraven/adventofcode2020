use std::collections::HashMap;
use std::io::BufRead;
use std::num::ParseIntError;
use std::str::FromStr;

mod lib;

#[derive(Debug)]
enum Command {
    Mask { zeros: u64, ones: u64 },
    Mem { addr: u64, value: u64 },
}

impl FromStr for Command {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let sepd = input.split(" = ").collect::<Vec<_>>();
        Ok(match &sepd[0][0..4] {
            "mem[" => Command::Mem {
                addr: sepd[0][4..].strip_suffix("]").unwrap().parse()?,
                value: sepd[1].parse()?,
            },
            "mask" => Command::Mask {
                zeros: (0..)
                    .zip(sepd[1].chars().rev())
                    .filter(|(_, c)| *c == '0')
                    .fold(0, |f, (i, _)| f | (1 << i)),
                ones: (0..)
                    .zip(sepd[1].chars().rev())
                    .filter(|(_, c)| *c == '1')
                    .fold(0, |f, (i, _)| f | (1 << i)),
            },
            _ => panic!("unknown command {}", input),
        })
    }
}

fn load() -> Vec<Command> {
    lib::load("14")
        .unwrap()
        .lines()
        .map(|l| Command::from_str(&l.unwrap()).unwrap())
        .collect()
}

fn execute(cmds: &Vec<Command>) -> HashMap<u64, u64> {
    let mut zero_mask = 0;
    let mut ones_mask = 0;
    let mut mem = HashMap::new();

    for cmd in cmds {
        match cmd {
            Command::Mask { zeros, ones } => {
                zero_mask = *zeros;
                ones_mask = *ones;
            }
            Command::Mem { addr, value } => {
                mem.insert(*addr, (value & !zero_mask) | ones_mask);
            }
        }
    }
    mem
}

fn main() {
    let cmds = load();
    let mem = execute(&cmds);
    println!("Memory sum after execution: {}", mem.values().sum::<u64>());
}
