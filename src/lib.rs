use std::fs::File;
use std::io::{BufReader, Read};

pub fn open(day: &str) -> Result<std::fs::File, std::io::Error> {
    File::open(format!("inputs/{}", day))
}

pub fn load(day: &str) -> Result<std::io::BufReader<std::fs::File>, std::io::Error> {
    let file = open(day)?;
    Ok(BufReader::new(file))
}

pub fn load_all(day: &str) -> Result<String, std::io::Error> {
    let mut str = String::new();
    open(day)?.read_to_string(&mut str)?;
    Ok(str)
}
