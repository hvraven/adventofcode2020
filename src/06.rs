use std::collections::HashSet;

mod lib;

fn main() {
    let input = lib::load_all("06").unwrap();
    let any_answers = input
        .split("\n\n")
        .map(|block| {
            block
                .split_whitespace()
                .flat_map(|s| s.chars())
                .collect::<HashSet<_>>()
        })
        .collect::<Vec<_>>();
    println!(
        "Sum of unique answers per group: {}",
        any_answers.iter().map(|set| set.len()).sum::<usize>()
    );

    let all_answers = input
        .split("\n\n")
        .map(|block| {
            block
                .split_whitespace()
                .map(|s| s.chars().fold(0, |b, c| b | (1 << (c as u32 - 'a' as u32))))
                .fold(u32::MAX, |a, b| a & b)
                .count_ones()
        })
        .collect::<Vec<_>>();
    println!(
        "Sum fo all common answers per group {}",
        all_answers.iter().sum::<u32>()
    );
}
