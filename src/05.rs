mod lib;
use std::io::prelude::*;

#[derive(Debug)]
enum Error {
    ParseError,
}

struct Seat {
    row: u32,
    col: u32,
}

impl Seat {
    fn from_ticket(input: &str) -> Result<Self, Error> {
        let rows: Vec<_> = (0..128).collect();
        let cols: Vec<_> = (0..8).collect();
        let mut row: &[u32] = &rows;
        let mut col: &[u32] = &cols;
        for c in input.chars() {
            match c {
                'F' => row = row.split_at(row.len() / 2).0,
                'B' => row = row.split_at(row.len() / 2).1,
                'L' => col = col.split_at(col.len() / 2).0,
                'R' => col = col.split_at(col.len() / 2).1,
                _ => return Err(Error::ParseError),
            }
        }

        Ok(Seat {
            row: row[0],
            col: col[0],
        })
    }

    fn id(&self) -> u32 {
        self.row * 8 + self.col
    }
}

fn load() -> Vec<Seat> {
    lib::load("05")
        .unwrap()
        .lines()
        .map(|s| Seat::from_ticket(&s.unwrap()))
        .collect::<Result<_, _>>()
        .unwrap()
}

fn main() {
    let seats = load();
    let ids = seats.iter().map(|s| s.id()).collect::<Vec<_>>();
    let max_id = ids.iter().max().unwrap();
    let min_id = ids.iter().min().unwrap();
    println!("Highest seat id: {}", max_id);
    println!(
        "My seat id: {}",
        (max_id + min_id) * (seats.len() as u32 + 1) / 2 - ids.iter().sum::<u32>()
    );
}
