use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::io::BufRead;
use std::str::FromStr;

mod lib;

#[derive(Copy, Clone, Debug, FromPrimitive)]
enum Direction {
    N = 0,
    S = 2,
    E = 1,
    W = 3,
}

impl Direction {
    fn rotate(self, degrees: i32) -> Self {
        FromPrimitive::from_i32((self as i32 + 4 + degrees / 90) % 4).unwrap()
    }
}

#[derive(Debug)]
enum Command {
    Move { dir: Direction, distance: i32 },
    Turn { degrees: i32 },
    Forward { distance: i32 },
}

impl FromStr for Command {
    type Err = std::num::ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let count = input[1..].parse::<i32>()?;

        Ok(match input.chars().nth(0).unwrap() {
            'N' => Command::Move {
                dir: Direction::N,
                distance: count,
            },
            'S' => Command::Move {
                dir: Direction::S,
                distance: count,
            },
            'E' => Command::Move {
                dir: Direction::E,
                distance: count,
            },
            'W' => Command::Move {
                dir: Direction::W,
                distance: count,
            },
            'L' => Command::Turn {
                degrees: -1 * count,
            },
            'R' => Command::Turn { degrees: count },
            'F' => Command::Forward { distance: count },
            _ => panic!("Unknown command character"),
        })
    }
}

#[derive(Debug)]
struct Boat {
    x: i32,
    y: i32,
    dir: Direction,
}

impl Boat {
    fn execute(self, cmd: &Command) -> Self {
        match cmd {
            Command::Move {
                dir: Direction::N,
                distance,
            } => Boat {
                y: self.y + distance,
                ..self
            },
            Command::Move {
                dir: Direction::S,
                distance,
            } => Boat {
                y: self.y - distance,
                ..self
            },
            Command::Move {
                dir: Direction::W,
                distance,
            } => Boat {
                x: self.x - distance,
                ..self
            },
            Command::Move {
                dir: Direction::E,
                distance,
            } => Boat {
                x: self.x + distance,
                ..self
            },
            Command::Turn { degrees } => Boat {
                dir: self.dir.rotate(*degrees),
                ..self
            },
            Command::Forward { distance } => {
                let cmd = Command::Move {
                    dir: self.dir.clone(),
                    distance: *distance,
                };
                self.execute(&cmd)
            }
        }
    }
}

#[derive(Debug)]
struct BoatWithWaypoint {
    boatx: i32,
    boaty: i32,
    wpx: i32,
    wpy: i32,
}

impl BoatWithWaypoint {
    fn execute(self, cmd: &Command) -> Self {
        match cmd {
            Command::Move {
                dir: Direction::N,
                distance,
            } => Self {
                wpy: self.wpy + distance,
                ..self
            },
            Command::Move {
                dir: Direction::S,
                distance,
            } => Self {
                wpy: self.wpy - distance,
                ..self
            },
            Command::Move {
                dir: Direction::W,
                distance,
            } => Self {
                wpx: self.wpx - distance,
                ..self
            },
            Command::Move {
                dir: Direction::E,
                distance,
            } => Self {
                wpx: self.wpx + distance,
                ..self
            },
            Command::Turn { degrees } => match (degrees + 360) % 360 {
                0 => self,
                90 => Self {
                    wpx: self.wpy,
                    wpy: -self.wpx,
                    ..self
                },
                180 => Self {
                    wpx: -self.wpx,
                    wpy: -self.wpy,
                    ..self
                },
                270 => Self {
                    wpx: -self.wpy,
                    wpy: self.wpx,
                    ..self
                },
                _ => panic!("Weird angle {}", degrees),
            },
            Command::Forward { distance } => Self {
                boatx: self.boatx + self.wpx * distance,
                boaty: self.boaty + self.wpy * distance,
                ..self
            },
        }
    }
}

fn main() {
    let commands = lib::load("12")
        .unwrap()
        .lines()
        .map(|s| Command::from_str(&s.unwrap()).unwrap())
        .collect::<Vec<_>>();

    let end_of_journey = commands.iter().fold(
        Boat {
            dir: Direction::E,
            x: 0,
            y: 0,
        },
        |boat, cmd| boat.execute(cmd),
    );

    println!("{:?}", end_of_journey);
    println!(
        "Final distance: {}",
        end_of_journey.x.abs() + end_of_journey.y.abs()
    );
    let end_with_waypoint = commands.iter().fold(
        BoatWithWaypoint {
            wpx: 10,
            wpy: 1,
            boatx: 0,
            boaty: 0,
        },
        |boat, cmd| boat.execute(cmd),
    );
    println!("{:?}", end_with_waypoint);
    println!(
        "Final distance with waypoint: {}",
        end_with_waypoint.boatx.abs() + end_with_waypoint.boaty.abs()
    )
}
