use nom::lib::std::collections::HashSet;
use std::io::BufRead;
use std::num::ParseIntError;
use std::str::FromStr;

mod lib;

#[derive(Clone, Copy, Debug)]
enum Command {
    Acc { acc: i32 },
    Jmp { offset: i32 },
    Nop { dnc: i32 },
}

impl FromStr for Command {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        Ok(match &input[0..3] {
            "acc" => Command::Acc {
                acc: input[4..].parse()?,
            },
            "jmp" => Command::Jmp {
                offset: input[4..].parse()?,
            },
            "nop" => Command::Nop {
                dnc: input[4..].parse()?,
            },
            _ => panic!("Unknown command {}", input),
        })
    }
}

fn load() -> Vec<Command> {
    lib::load("08")
        .unwrap()
        .lines()
        .flat_map(|s| Command::from_str(&s.unwrap()))
        .collect()
}

fn execute_one(cmds: &Vec<Command>) -> i32 {
    let mut seen = HashSet::new();
    let mut i: i32 = 0;
    let mut acc = 0;
    while !seen.contains(&i) {
        seen.insert(i);
        match cmds.get(i as usize) {
            Some(Command::Acc { acc: v }) => acc += v,
            Some(Command::Jmp { offset }) => i += offset - 1,
            Some(Command::Nop { dnc }) => (),
            None => return acc,
        }
        i += 1
    }
    acc
}

fn execute_two(cmds: &Vec<Command>) -> Option<i32> {
    let mut seen = HashSet::new();
    let mut i: i32 = 0;
    let mut acc = 0;
    while !seen.contains(&i) {
        seen.insert(i);
        match cmds.get(i as usize) {
            Some(Command::Acc { acc: v }) => acc += v,
            Some(Command::Jmp { offset }) => i += offset - 1,
            Some(Command::Nop { dnc }) => (),
            None => return Some(acc),
        }
        i += 1
    }
    None
}

fn part_two(mut cmds: Vec<Command>) -> i32 {
    for i in 0..cmds.len() {
        let orig = cmds[i].clone();
        let replace = match orig {
            Command::Acc { acc } => orig,
            Command::Jmp { offset } => Command::Nop { dnc: offset },
            Command::Nop { dnc } => Command::Jmp { offset: dnc },
        };
        cmds[i] = replace;
        if let Some(acc) = execute_two(&cmds) {
            return acc;
        }
        cmds[i] = orig;
    }
    panic!("did not find a valid code");
}

fn main() {
    let cmds = load();
    println!("Acc on first loop: {}", execute_one(&cmds));
    println!("Acc on fixed code: {}", part_two(cmds));
}
