use derive_more::From;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;

mod lib;

#[derive(Debug, From)]
enum Error {
    MissingFieldError,
    ValidationError,
    IntError(std::num::ParseIntError),
}

#[derive(Debug)]
struct Passport {
    byr: u32,
    iyr: u32,
    eyr: u32,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: Option<String>,
}

impl Passport {
    fn valid(&self) -> bool {
        fn _valid(p: &Passport) -> Result<(), Error> {
            for (val, min, max) in &[
                (p.byr, 1920, 2002),
                (p.iyr, 2010, 2020),
                (p.eyr, 2020, 2030),
            ] {
                if val < min || val > max {
                    return Err(Error::ValidationError);
                }
            }

            for (suffix, min, max) in &[("in", 59, 76), ("cm", 150, 193)] {
                if p.hgt.ends_with(suffix) {
                    let size = p
                        .hgt
                        .strip_suffix(suffix)
                        .ok_or(Error::ValidationError)?
                        .parse::<u32>()?;
                    if size < *min || size > *max {
                        return Err(Error::ValidationError);
                    }
                }
            }
            if !(p.hgt.ends_with("in") || p.hgt.ends_with("cm")) {
                return Err(Error::ValidationError);
            }

            if p.pid.len() != 9 {
                return Err(Error::ValidationError);
            } else {
                p.pid.parse::<u32>()?;
            }

            if !p.hcl.starts_with("#") && p.hcl.len() != 7 {
                return Err(Error::ValidationError);
            }
            u32::from_str_radix(p.hcl.trim_start_matches("#"), 16)?;

            let mut colors: HashSet<String> = HashSet::new();
            for color in &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] {
                colors.insert(color.to_string());
            }
            if !colors.contains(&p.ecl) {
                return Err(Error::ValidationError);
            }

            Ok(())
        }
        _valid(&self).is_ok()
    }
}

impl FromStr for Passport {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut components = HashMap::new();
        for component in input.split_whitespace() {
            let parts: Vec<&str> = component.split(":").collect();
            components.insert(parts[0], parts[1]);
        }
        Ok(Passport {
            byr: components
                .get("byr")
                .ok_or(Error::MissingFieldError)?
                .parse()?,
            iyr: components
                .get("iyr")
                .ok_or(Error::MissingFieldError)?
                .parse()?,
            eyr: components
                .get("eyr")
                .ok_or(Error::MissingFieldError)?
                .parse()?,
            hgt: components
                .get("hgt")
                .ok_or(Error::MissingFieldError)?
                .to_string(),
            hcl: components
                .get("hcl")
                .ok_or(Error::MissingFieldError)?
                .to_string(),
            ecl: components
                .get("ecl")
                .ok_or(Error::MissingFieldError)?
                .to_string(),
            pid: components
                .get("pid")
                .ok_or(Error::MissingFieldError)?
                .to_string(),
            cid: components.get("cid").map(|s| s.to_string()),
        })
    }
}

fn main() {
    let passports: Vec<Passport> = lib::load_all("04")
        .unwrap()
        .split("\n\n")
        .filter_map(|s| Passport::from_str(s).ok())
        .collect();
    println!("\"Valid\" passports: {}", passports.len());
    println!(
        "Really \"valid\" passports: {}",
        passports.iter().map(|p| p.valid() as u32).sum::<u32>()
    );
}
